import { Component } from '@angular/core';
import {MatSelectChange} from '@angular/material';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  view: number[] = [undefined, undefined]; // auto fit for chart doesn't work correctly, so next code is solution

  dataOptions = [{ name: 'Data with missed hours', value: 'data_with_missed_hours' }, { name: 'Data without missed hours', value: 'data_without_missed_hours' }];

  results = [];

  results_without_missed_hours = [
    {
      name: 'Sunday',
      series: [
        { name: '7 pm', value: 86.2 },
        { name: '6 pm', value: 86.2 },
        { name: '5 pm', value: 86.3 },
        { name: '4 pm', value: 86.2 },
        { name: '3 pm', value: 86.4 },
        { name: '2 pm', value: 86.1 },
        { name: '1 pm', value: 86.2 },
        { name: '12 pm', value: 85.6 },
        { name: '11 am', value: 86.1 },
        { name: '10 am', value: 85.3 },
        { name: '9 am', value: 85.39999999999999 },
        { name: '8 am', value: 85.8 },
      ],
    },
    {
      name: 'Monday',
      series: [
        { name: '7 pm', value: 86.4 },
        { name: '6 pm', value: 86.3 },
        { name: '5 pm', value: 86.2 },
        { name: '4 pm', value: 86.5 },
        { name: '3 pm', value: 86.3 },
        { name: '2 pm', value: 86.3 },
        { name: '1 pm', value: 86 },
        { name: '12 pm', value: 86.1 },
        { name: '11 am', value: 85.7 },
        { name: '10 am', value: 86.1 },
        { name: '9 am', value: 85.6 },
        { name: '8 am', value: 85.6 },
      ],
    },
    {
      name: 'Tuesday',
      series: [
        { name: '7 pm', value: 86.5 },
        { name: '6 pm', value: 86.3 },
        { name: '5 pm', value: 86.3 },
        { name: '4 pm', value: 86.5 },
        { name: '3 pm', value: 85.7 },
        { name: '2 pm', value: 86.2 },
        { name: '1 pm', value: 86.2 },
        { name: '12 pm', value: 86 },
        { name: '11 am', value: 85.9 },
        { name: '10 am', value: 85.9 },
        { name: '9 am', value: 86.6 },
        { name: '8 am', value: 86.3 },
      ],
    },
    {
      name: 'Wednesday',
      series: [
        { name: '7 pm', value: 86.3 },
        { name: '6 pm', value: 85.39999999999999 },
        { name: '5 pm', value: 86.5 },
        { name: '4 pm', value: 86 },
        { name: '3 pm', value: 86.5 },
        { name: '2 pm', value: 86.1 },
        { name: '1 pm', value: 85.9 },
        { name: '12 pm', value: 85.6 },
        { name: '11 am', value: 86.1 },
        { name: '10 am', value: 86.1 },
        { name: '9 am', value: 86.9 },
        { name: '8 am', value: 86.4 },
      ],
    },
    {
      name: 'Thursday',
      series: [
        { name: '7 pm', value: 86.1 },
        { name: '6 pm', value: 86.1 },
        { name: '5 pm', value: 86.3 },
        { name: '4 pm', value: 86.6 },
        { name: '3 pm', value: 86.8 },
        { name: '2 pm', value: 86 },
        { name: '1 pm', value: 86 },
        { name: '12 pm', value: 86.1 },
        { name: '11 am', value: 86 },
        { name: '10 am', value: 86.2 },
        { name: '9 am', value: 86.2 },
        { name: '8 am', value: 86.3 },
      ],
    },
  ];

  results_with_missed_hours = [
    {
      name: 'Sunday',
      series: [
        { name: '7 pm', value: 86.2 },
        { name: '6 pm', value: 86.2 },
        { name: '5 pm', value: 86.3 },
        // { name: '4 pm', value: 86.2 },
        // { name: '3 pm', value: 86.4 },
        { name: '2 pm', value: 86.1 },
        { name: '1 pm', value: 86.2 },
        { name: '12 pm', value: 85.6 },
        { name: '11 am', value: 86.1 },
        { name: '10 am', value: 85.3 },
        { name: '9 am', value: 85.39999999999999 },
        { name: '8 am', value: 85.8 },
      ],
    },
    {
      name: 'Monday',
      series: [
        { name: '7 pm', value: 86.4 },
        { name: '6 pm', value: 86.3 },
        { name: '5 pm', value: 86.2 },
        { name: '4 pm', value: 86.5 },
        { name: '3 pm', value: 86.3 },
        { name: '2 pm', value: 86.3 },
        { name: '1 pm', value: 86 },
        { name: '12 pm', value: 86.1 },
        // { name: '11 am', value: 85.7 },
        // { name: '10 am', value: 86.1 },
        { name: '9 am', value: 85.6 },
        { name: '8 am', value: 85.6 },
      ],
    },
    {
      name: 'Tuesday',
      series: [
        { name: '7 pm', value: 86.5 },
        { name: '6 pm', value: 86.3 },
        { name: '5 pm', value: 86.3 },
        { name: '4 pm', value: 86.5 },
        { name: '3 pm', value: 85.7 },
        { name: '2 pm', value: 86.2 },
        { name: '1 pm', value: 86.2 },
        { name: '12 pm', value: 86 },
        { name: '11 am', value: 85.9 },
        { name: '10 am', value: 85.9 },
        // { name: '9 am', value: 86.6 },
        // { name: '8 am', value: 86.3 },
      ],
    },
    {
      name: 'Wednesday',
      series: [
        // { name: '7 pm', value: 86.3 },
        // { name: '6 pm', value: 85.39999999999999 },
        { name: '5 pm', value: 86.5 },
        { name: '4 pm', value: 86 },
        { name: '3 pm', value: 86.5 },
        { name: '2 pm', value: 86.1 },
        { name: '1 pm', value: 85.9 },
        { name: '12 pm', value: 85.6 },
        { name: '11 am', value: 86.1 },
        { name: '10 am', value: 86.1 },
        { name: '9 am', value: 86.9 },
        { name: '8 am', value: 86.4 },
      ],
    },
    {
      name: 'Thursday',
      series: [
        { name: '7 pm', value: 86.1 },
        { name: '6 pm', value: 86.1 },
        { name: '5 pm', value: 86.3 },
        { name: '4 pm', value: 86.6 },
        { name: '3 pm', value: 86.8 },
        { name: '2 pm', value: 86 },
        { name: '1 pm', value: 86 },
        { name: '12 pm', value: 86.1 },
        { name: '11 am', value: 86 },
        { name: '10 am', value: 86.2 },
        { name: '9 am', value: 86.2 },
        { name: '8 am', value: 86.3 },
      ],
    },
  ];

  colorScheme = {
    name: 'vivid',
    selectable: true,
    group: 'Ordinal',
    domain: ['#e1f5fe', '#b3e5fc', '#81d4fa', '#4fc3f7', '#29b6f6', '#03a9f4', '#039be5', '#0288d1', '#0277bd', '#01579b'],
  };

  constructor() {
    this.results = this.results_with_missed_hours;
  }

  onSelectionChange(event: MatSelectChange): void {
    switch (event.value) {
      case 'data_without_missed_hours':
        this.results = this.results_without_missed_hours;
        break;
      default:
        this.results = this.results_with_missed_hours;
    }

    console.log(event.value, this.results);
  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PulseChartsHeapMapComponent } from './pulse-charts-heat-map.component';

describe('PulseChartsHeapMapComponent', () => {
  let component: PulseChartsHeapMapComponent;
  let fixture: ComponentFixture<PulseChartsHeapMapComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PulseChartsHeapMapComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PulseChartsHeapMapComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

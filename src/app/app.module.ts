import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatSelectModule} from '@angular/material';

import { AppComponent } from './app.component';
import { PulseChartsHeatMapComponent } from './pulse-charts-heat-map/pulse-charts-heat-map.component';

@NgModule({
  declarations: [AppComponent, PulseChartsHeatMapComponent],
  imports: [BrowserModule, BrowserAnimationsModule, NgxChartsModule, MatSelectModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
